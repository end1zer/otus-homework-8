﻿using homework8.Entities.Swords;
using Newtonsoft.Json;

RustyBluntSword rustyBluntSword = new(10.0, 15.0, "Boromir");

var rustyBluntSwordMyClone = rustyBluntSword.MyClone();
rustyBluntSwordMyClone.SetHolder("Faromir");

var rustyBluntSwordClone = (RustyBluntSword)rustyBluntSword.Clone();
rustyBluntSwordClone.SetHolder("Gimli");

Console.WriteLine(JsonConvert.SerializeObject(rustyBluntSword));
Console.WriteLine(JsonConvert.SerializeObject(rustyBluntSwordMyClone));
Console.WriteLine(JsonConvert.SerializeObject(rustyBluntSwordClone));
