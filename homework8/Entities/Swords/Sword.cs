﻿using homework8.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework8.Entities.Swords
{
    /// <summary>
    /// Меч
    /// </summary>
    internal class Sword : IMyCloneable<Sword>, ICloneable
    {
        /// <summary>
        /// Режущая атака
        /// </summary>
        public double CuttingAttack { get; set; }

        /// <summary>
        /// Колющая атака
        /// </summary>
        public double StabbingAttack { get; set; }

        /// <summary>
        /// Имя владельца меча
        /// </summary>
        /// <remarks>
        /// Просто для теста :)
        /// </remarks>
        public string? Holder { get; set; }

        public Sword(Sword sword)
        {
            CuttingAttack = sword.CuttingAttack;
            StabbingAttack = sword.StabbingAttack;
        }

        public Sword(double cuttingAttack, double stabbingAttack, string holder)
        {
            CuttingAttack = cuttingAttack;
            StabbingAttack = stabbingAttack;
            Holder = holder;
        }

        public virtual object Clone()
        {
            return MemberwiseClone();
        }

        public virtual Sword MyClone()
        {
            return new Sword(this);
        }

        public void SetHolder(string holder)
        {
            Holder = holder;
        }
    }
}
