﻿using homework8.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework8.Entities.Swords
{
    /// <summary>
    /// Ржавый тупой меч
    /// </summary>
    internal class RustyBluntSword : RustySword, IMyCloneable<RustyBluntSword>, ICloneable
    {
        public RustyBluntSword(double cuttingAttack, double stabbingAttack, string holder)
            : base(cuttingAttack, stabbingAttack, holder)
        {

        }

        public RustyBluntSword(RustyBluntSword rustyBluntSword)
            : base(rustyBluntSword)
        {

        }

        public override RustyBluntSword MyClone()
        {
            return new RustyBluntSword(this);
        }
    }
}
