﻿using homework8.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework8.Entities.Swords
{
    /// <summary>
    /// Ржавый меч
    /// </summary>
    internal class RustySword : Sword, IMyCloneable<RustySword>, ICloneable
    {
        public RustySword(double cuttingAttack, double stabbingAttack, string holder)
            : base(cuttingAttack, stabbingAttack, holder)
        {

        }

        public RustySword(RustySword rustySword)
            : base(rustySword)
        {

        }

        public override RustySword MyClone()
        {
            return new RustySword(this);
        }
    }
}
