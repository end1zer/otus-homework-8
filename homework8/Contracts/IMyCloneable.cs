﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework8.Contracts
{
    /// <summary>
    /// Кастомный интерфейс клонирования
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal interface IMyCloneable<T>
        where T : class
    {
        T MyClone();
    }
}
